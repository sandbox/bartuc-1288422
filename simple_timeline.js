(function ($) {	
	$(document).ready(function() {
	  //Timeline
		$('div.timeline-middle span.timeline-icon a').click(function() {
			var id = $(this).parent().attr('class').replace(/^.*?([0-9]{1,3}).*/, '$1');
			var div = $('span.timeline-bubble-' + id);
			var bottom = $('span.timeline-bubble-bottom-' + id);
			var all = $('span.timeline-bubble');
			var allBottom = $('span.timeline-bubble-bottom');
			
			if(div.hasClass('hide')) {
			  all.addClass('hide');
			  allBottom.addClass('hide');
				div.removeClass('hide');
				bottom.removeClass('hide');
			}
			else {
			  all.addClass('hide');
			  allBottom.addClass('hide'); 
			}
		});

	// Exit button on each timeline bubble
	$('div.timeline-wrapper span.exit').click(function() {
		var id = $('div.timeline-middle span.timeline-icon a').parent().attr('class').replace(/^.*?([0-9]{1,3}).*/, '$1');
		var div = $('span.timeline-bubble-' + id);
		var bottom = $('span.timeline-bubble-bottom-' + id);
		var all = $('span.timeline-bubble');
		var allBottom = $('span.timeline-bubble-bottom');
		all.addClass('hide');
		allBottom.addClass('hide');
	});
	
	// Timeline buttons
		
    // Timeline button auto scroll right
		  var icon = $('div.timeline-middle');
		  var icon_css = $(icon).css('left').replace(/^.*?([-][0-9]{1,3}).*/, '$1');

	  var show = $('div.timeline-show ul');
	  var showWidth = $(show).css('width').replace(/^.*?([0-9]{1,5}).*/, '$1');
      var left = 0;
	  var width = $('div.timeline-wrapper').width();
      var limit = (parseInt(showWidth) - width) * -1;
	  var timeout, clicker = $('div.timeline-bottom div.button-right img');
      clicker.mouseover(function(){
        timeout = setInterval(function(){
			if(left <= limit) {
	          left = limit;
	        }		  
	        else {
	          left = parseInt(left) - 15;
	          icon_css = parseInt(icon_css) - 15;		
	          $(show.css('left', left + 'px'));
	          $(icon.css('left', icon_css + 'px')); 
	        }		  
        }, 50);

        return false;
      });
      $('div.timeline-bottom div.button-right img').mouseout(function() {
        clearInterval(timeout);
        return false;
      });

	    // Timeline button auto scroll left
	    var timeout, clicker = $('div.timeline-bottom div.button-left img');

      clicker.mouseover(function(){
        timeout = setInterval(function(){
          if(left >= 0) {
	          left = 0;
	        }		  
	        else {
	          left = parseInt(left) + 15;            
	          icon_css = parseInt(icon_css) + 15;	
	          $(show.css('left', left + 'px'));
	          $(icon.css('left', icon_css + 'px'));  
	        }		  
        }, 50);

        return false;
      });      
      $('div.timeline-bottom div.button-left img').mouseout(function() {
        clearInterval(timeout);
        return false;
      });

	  // Timeline holding down the scroll bar
	
	
	 /* var icon = $('div.timeline-middle');
	  var show = $('div.timeline-show ul');	
	  var clicking = false; // Make sure the user is holding down the mouse while scrolling
	  var xstart = 0;       // Starting x coordinate
	  var xcheck = xstart;  // Finds how far the mouse moved in pixels using xstart -> current position
	  var difference = 0;   // How much the bar should be moving and from what side
	  var limit = (parseInt(showWidth) - 940) * -1;  // 940 = Hardcoded width of timeline
	  var left = $('div.timeline-bottom ul').css('left').replace(/^.*?([0-9]{1,5})./, '$1');
	  
	  var scrollout, clicker = $('div.timeline-bottom ul');
	  clicker.mousemove(function(e) {
	  	
      if(clicking == true) {
      
        timeout = setInterval(function(){
          if(xstart == 0 || xcheck == xstart) {
            xstart = e.pageX;
          }
          if((parseInt(left) - parseInt(difference)) > 0) {
            left = 0;
          }
          else if((parseInt(left) - parseInt(difference)) <= limit) {
            left = parseInt(limit) + 1;
          }
          else {
            difference = (parseInt(xstart) - parseInt(e.pageX));
            $(show.css('left', left - difference + 'px'));
            $(icon.css('left', left - difference + 'px'));
            $("div.timeline-middle").text("Start: " + xstart + " end: " + e.pageX).css("color", "#ffffff");
          }
        });
      }
	  });
	  $('div.timeline-bottom ul').mousedown(function() {
	    clicking = true;
	  });	  
	  $('div.timeline-bottom ul').mouseup(function() {
	      clearInterval(timeout);
	      clicking = false;
	      xstart = 0;
	    if((parseInt(left) - parseInt(difference)) > 0) {
	      left = 0;
	    }
	    else if((parseInt(left) - parseInt(difference)) <= limit) {
	      left = parseInt(limit) + 1;
	    }
	    else {
	      left = parseInt(left) - parseInt(difference);
	    }
	    $(show.css('left', left + 'px'));
	    $(icon.css('left', left + 'px')); 	    
	    difference = 0;
	  }); */

	});
})(jQuery);
