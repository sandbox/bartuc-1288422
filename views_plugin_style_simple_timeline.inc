<?php
/**
 * @file
 * Contains the default style plugin.
 */

/**
 * Default style plugin to render rows one after another with no
 * decorations.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_simple_timeline extends views_plugin_style {
  /**
   * Set default options
   */
  function options(&$options) {
    $options['height'] = array('default' => '300px');
    $options['width'] = array('default' => '100%');
    $options['title_main'] = array('default' => 'The History');
    $options['title_secondary'] = array('default' => 'of simple time elements');
    $options['body'] = array('default' => 'Bacon Ipsum strip steak beef ground round ball tip biltong short loin. Swine brisket ribeye, pork strip steak ham hock meatball pork loin.');
    $options['fields']['title'] = array('default' => '');
    $options['fields']['time'] = array('default' => '');
    $options['fields']['node_body'] = array('default' => '');
    $options['timeline']['year_start'] = array('default' => '1700');
    $options['timeline']['year_end'] = array('default' => '2100');
    $options['timeline']['increment'] = array('default' => '10');
    return $options;
  }
  
  /**
   * Implemention of the style's options form
   */
  function options_form(&$form, &$form_state) {
    $field_names = array('' => '--');   
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => '30',
      '#description' => t('This field determines how tall the timeline will be'),
      '#default_value' => $this->options['height'],
    );
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => '30',
      '#description' => t('This field determines how wide the timeline will be'),
      '#default_value' => $this->options['width'],   
    );
    $form['title_main'] = array(
      '#type' => 'textfield',
      '#title' => t('Main Title'),
      '#size' => '30',
      '#description' => t('Uses the larger title font on the timeline'),
      '#default_value' => $this->options['title_main'],   
    );
    $form['title_secondary'] = array(
      '#type' => 'textfield',
      '#title' => t('Secondary Title'),
      '#size' => '30',
      '#description' => t('Uses the smaller title font on the timeline'),
      '#default_value' => $this->options['title_secondary'],   
    );
    $form['body'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#description' => t('A short description'),
      '#default_value' => $this->options['body'],   
    );
    $form['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Fields'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $handlers = $this->display->handler->get_handlers('field');
    foreach ($handlers AS $field => $handler) {
      $field_name[$field] = $handler->ui_name();
    }
    $form['fields']['title'] = array(
      '#type' => 'select',
      '#title' => t('Title'),
      '#description' => t('Choose what field should be mapped as the title for each node in the timeline'),
      '#options' => $field_name,
      '#default_value' => $this->options['fields']['title'],
    );
    $form['fields']['node_body'] = array(
      '#type' => 'select',
      '#title' => t('Body'),
      '#description' => t('A short description about the node. Max length is 150 characters. Any extra will be cut off in order to fit in the bubble.'),
      '#options' => $field_name,
      '#default_value' => $this->options['fields']['node_body'],      
    );
    $form['fields']['time'] = array(
      '#type' => 'select',
      '#title' => t('Date Field'),
      '#description' => t('Choose what field should be mapped as the date in the timeline. Use just a year, ex 1987 or a full date, ex 1987-05-08 00:00:00'),
      '#options' => $field_name,
      '#default_value' => $this->options['fields']['time'],
    );
    
    // Time options
    $form['timeline'] = array(
      '#type' => 'fieldset',
      '#title' => t('Time Options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['timeline']['year_start'] = array(
      '#type' => 'textfield',
      '#title' => t('Starting Year'),
      '#description' => t('The earliest year your timeline should scroll too. Ex if showing the 20th century, this should be 1900'),
      '#default_value' => $this->options['timeline']['year_start'],
      '#size' => '5',
      '#maxlength' => '5',
    );
    $form['timeline']['year_end'] = array(
      '#type' => 'textfield',
      '#title' => t('Ending Year'),
      '#description' => t('The latest year your timeline should scroll too. Ex if showing the 20th century, this should be 2000'),
      '#default_value' => $this->options['timeline']['year_end'],
      '#size' => '5',
      '#maxlength' => '5',      
    );
    $form['timeline']['increment'] = array(
      '#type' => 'textfield',
      '#title' => t('Increment (years)'),
      '#description' => t('How many years each point should increment. Ex 10 would show 1900 1910 1920 etc'),
      '#default_value' => $this->options['timeline']['increment'],
      '#size' => '5',
      '#maxlength' => '5',      
    );        
  } 
}
