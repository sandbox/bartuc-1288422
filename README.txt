Simple Timeline Module created by Bartuc, 09-18-2011

About Simple Timeline Module
	The Simple Timeline Module is just as the title states, simple. All you have to do is enable
	the module and you will have an option to choose the simple timeline style. You can choose 
	different date ranges, increments, and fields to plug into the output. As of version 1.0
	you can only choose whole year values such as 1900, 1950, etc. Multiple nodes may contain
	the same date, as they will stack, but the timeline defaults to only stack three dates
	comfortably. If you need more dates for a single year, just increase the timelines height.
	
	The timeline module is very customizable as you can alter much of it through CSS. The
	JavaScript is also located inside of the module so you can easily add your own custom
	scripting. Frequently check for updates and releases for more advanced functionality.

Installation Instructions
 - Download Simple Timeline to your modules folder
 - Enable the module

Getting Started
 - Create a view
 - Choose the style "Simple Timeline"
 - Set up the timeline through the style settings
 - Make sure you map a Title, Body, and most importantly a Date.
 - Save view and visit your page/block
