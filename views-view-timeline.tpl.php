<?php
// $Id: views_view_timeline.tpl.php,v 1.0 2011/08/19 bartuc Exp $

/**
 * @file
 * This template includes the timeline view.
 */
drupal_add_js(drupal_get_path('module', 'simple_timeline') . '/simple_timeline.js');
?>

<div class="timeline-wrapper timeline-wrapper-<?php print $display; ?>" style="width: <?php print $width; ?>; max-width: <?php print $max_width; ?>px;">
  <noscript><?php print t('This page uses Javascript to show you a Timeline. Please enable Javascript in your browser to see the full page. Thank you.'); ?></noscript>
  <div id="timeline">
    <div class="timeline-top" style="height: <?php print $height - 50; ?>px">
      <div class="main-title"><?php print $main_title; ?></div>
      <div class="second-title"><?php print $second_title; ?></div>
      <div class="body"><?php print $body; ?></div>
    </div>
    <div class="timeline-middle">
      <?php if ($rows): ?>
        <div class="view-content">
          <?php $path = drupal_get_path('module', 'simple_timeline'); ?>
          <?php $counter = 0; ?>
          <?php foreach ($rows as $id => $row): ?>
		    <?php if(in_array($row->field_field_date[0]['raw']['value'], $time_dupe)): ?>
			  <?php $extention = $extention -30; ?>
		    <?php else: ?>
			  <?php $time_dupe[$row->field_field_date[0]['raw']['value']] = $row->field_field_date[0]['raw']['value']; ?>
			  <?php $extention = 0; ?>
		    <?php endif; ?>
            <?php if($times[$counter] >= $start && $times[$counter] <= $end): ?>
              <?php $left = ($times[$counter] - $start) * ($i_width/$increment) + 27; ?> <!-- the 27 is half of the li (70/2 = 35) - length of half of tick image (17)-->
              <div class="bubble-wrapper">
                <?php if((135 - $extention) > ($height - 50)): ?>
                  <?php $bubble_difference = (135 - $extention) - ($height - 50); ?>
                  <?php $bubble_height = 96 - $bubble_difference; ?>
                  <?php if(($left + 380) < $max_width): ?>
                      <span class="hide timeline-bubble timeline-bubble-<?php print $counter; ?>" style="height: <?php print $bubble_height; ?>px; width: 380px; position: absolute; left: <?php print $left; ?>px; margin-top: <?php print (-135 + $extention + $bubble_difference);?>px;">
                  <?php else: ?>
                    <?php $left = $left - 360; ?>
                    <span class="hide timeline-bubble timeline-bubble-<?php print $counter; ?>" style="height: <?php print $bubble_height; ?>px; width: 360; position: absolute; left: <?php print $left; ?>px; width: 380px; margin-top: <?php print (-135 + $extention + $bubble_difference);?>px;">         
                    <?php $left = $left + 360; ?>
                  <?php endif; ?>              
                <?php else: ?>        
                  <?php if(($left + 380) < $max_width): ?>
                      <span class="hide timeline-bubble timeline-bubble-<?php print $counter; ?>" style="position: absolute; left: <?php print $left; ?>px; margin-top: <?php print (-135 + $extention);?>px;">
                  <?php else: ?>
                    <?php $left = $left - 360; ?>
                    <span class="hide timeline-bubble timeline-bubble-<?php print $counter; ?>" style="position: absolute; left: <?php print $left; ?>px; width: 380px; margin-top: <?php print (-135 + $extention);?>px;">         
                    <?php $left = $left + 360; ?>
                  <?php endif; ?>
                <?php endif; ?>
                <span class="exit" title="close">[X]</span>
                <?php if($times): ?>
                  <span class="date"><?php print $times[$counter] . ' - '; ?></span>
                <?php endif; ?>
                <?php if($titles): ?>
                 <span class="title"><?php print $titles[$counter]; ?></span>
                <?php endif; ?>
                <?php if($node_body): ?>
                <br />
                  <span class="body"><?php print $node_body[$counter]; ?></span>
                <?php endif; ?> 
               </span>
                <span class="hide timeline-bubble-bottom timeline-bubble-bottom-<?php print $counter; ?>" style="position: absolute; left: <?php print $left; ?>px; margin-top: <?php print (-20 + $extention);?>px;">
                </span>
              </div>
            <span class="timeline-icon timeline-icon-<?php print $counter; ?>" style="position: absolute; left: <?php print $left - 5; ?>px; top: <?php print $extention; ?>px">
              <a title="<?php print $titles[$counter]; ?>">
				&nbsp;
              </a>
            </span>
            <?php endif; ?>
            <?php $counter++; ?> 
          <?php endforeach; ?>
        </div>
      <?php elseif ($empty): ?>
        <div class="view-empty">
          <?php print $empty; ?>
        </div>
      <?php endif; ?> 
    </div>
  </div>
	<div class="timeline-bottom">
	  <div class="timeline-show">     
		<ul class="timeline-date-list" style="width: <?php print $max_width; ?>px">
		  <?php while($year <= ($end)): ?>
			<li class="timeline-date-line-item" style="float: left; width: <?php print $i_width; ?>px; height: 32px;"><?php print $year; ?></li>
			<?php $year += $increment; ?>
			<?php $total += $i_width; ?>
		  <?php endwhile; ?>
		</ul>
	  </div>
    <div class="button-left"><img src="/<?php print $path; ?>/images/timeline_button_left.png" /></div>
	  <div class="button-right"><img src="/<?php print $path; ?>/images/timeline_button_right.png" /></div>
	</div>
</div>


