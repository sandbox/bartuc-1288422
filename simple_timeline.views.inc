<?php

/**
 * Implements of hook_views_plugins(). Adds view types to views UI interface.
 */
function simple_timeline_views_plugins() {
  return array(
    'style' => array(
      'timeline' => array(
        'title' => t('Simple Timeline'),
        'help' => t('Displays content on a timeline'),
        'handler' => 'views_plugin_style_simple_timeline',
        'theme' => 'views_view_timeline',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );
}
